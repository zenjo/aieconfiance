# Aieconfiance

## Résume
Des services web « gratuits » ? Les entreprises seraient-elles tout à coup devenues philanthropes ? Heureusement, le pourquoi de cette gratuité de services logiciels internet - la collecte de données de profilage, commence à se répandre tout doucement dans le grand public ; on montre ici le fonctionnement de ce pourquoi, et on en démontre aussi la faisabilité.

Comment font-"ils" cela ? Quels sont les techniques utilisées ? Nous allons tenter d’ouvrir un peu la boite noire, dans les pages web surtout, mais dans les mails aussi.

On limite la technique au maximum, c.-à-d. qu’on verra ces techniques plus dans leur fonctionnement que dans leur complexité, afin que chacun puisse en appréhender et s’approprier les mécanismes. "On ne peut se éfendre que devant ce qu’on comprend"

Site web (démonstration & exercices) : https://aieconfiance.sebille.name/tracking/

## Outils
### Technique
- Tracking cookies
- Tracking sur les liens
- Web beacon
- Système de tracking
- Tracking dans les emails
### Défense (atelier)
*todo*

- de noscript à ???
- android, voir:
    - https://www.erenumerique.fr/29-applications-pirates-bannies-google-play-store-article-48055-09.html
    - https://blog.rolandl.fr/2016-02-21-les-permissions-sous-android-2-slash-6-les-differentes-permissions.html
    - https://developer.android.com/guide/topics/permissions/overview#perm-groups

### Aspects sociaux et politique
*todo*

- discussions générales, libres; du type "Qu'en pensez-vous ?"
- discussions éthiques; par exemple, "Un tracking non personnalisé est-il acceptable ?" - Argumenter les avis
- lecture, compréhension et jugement à propos de conditions d'utilisation; comme Mailchimp, Wix, ...
- Utilisation par les partis de logiciels type [Nationbuilder](https://nationbuilder.com/). Voir également [Programmes Nationbuilder](https://nationbuilder.com/software) et [article du soir](https://plus.lesoir.be/149366/article/2018-04-04/nationbuilder-loutil-de-mobilisation-militante-que-le-monde-politique-sarrache)

## Note technique
 1. Dépendance: wget La simulation de mailbot dépend de wget. Si il est absent, vous devez l'installer pour que ça fonctionne. 
 Il faut donc que celui-ci soit:
	- utilisable par php sur le serveur
	- accessible. Souvent, dans une distribution linux, wget va se trouver dans /usr/bin/. Sur mac, avec les macports, il a de bonne chance de se trouver dans /opt/local/bin/. Vérifiez le chemin de wget dans /mailbot/config.inc.php, variable '$wget_path'.

1. les fichiers tracking/donnees_collectees.txt et /mailbot/tmp doit être read/write pour le serveur web. 

1. une clé google est nécessaire pour la géolocalisation et doit être placée dans le fichier `tracking/api/k` comme suit:
```
<script src="https://maps.google.com/maps/api/js?key=..."></script>
```