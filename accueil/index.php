<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="fr">
<head>
<?php include ("../commun/inc-meta.php"); ?>
<title>Aie confiance!</title>
</head>
<body>
    <?php include ("../commun/inc-entete.php"); ?>
    <div class="encart"><h2>Aie confiance !</h2><p><img src="../commun/big-brother-reduit.jpg"></p></div>
    <h1>Aie confiance!</h1>
    <div>
        Petits programmes Internet pédagogiques à propos de choses cachées souvent ignorées, telles que collecte d'informations personnelles, tracking, collecte d'adresses mail, etc.
    </div>
    <ul>
        <li><a href="../tracking">Simulation de site "gratuit"</a> (Récolte d'informations personnalisées et tracking de préférence)</li>
        <li><a href="../mailbot">Collecteur d'adresses email</a> (y compris du type user{{a+T])domain(p-oi_nt]]tld)</li>
    </ul>

    <div class="note">NB: Au 27/09/2012, les noms de certains cookies ont changé pour les rendre plus explicites. Il vaut mieux avant de poursuivre effacer les cookies précédant cette date.</div>

    <h2>Suggestions de visite</h2>
    <ul>
        <li>Wikipedia <a href="http://fr.wikipedia.org/wiki/Cookie_(informatique)" target="_blank">Cookie (informatique)</a></li>
        <li>Le projet Mozilla <a href="http://www.mozilla.org/en-US/collusion/" target="_blank">Collusion</a> Discover who’s tracking you online (en)</li>
        <li>malekal's site (site entraide informatique) : <a
        href="http://www.malekal.com/2011/08/31/web-tracking-sur-internet/"
        target="_blank">Web Tracking sur internet</a>, <a
        href="http://www.malekal.com/2010/11/12/les-cookies-et-les-dangers/"
        target="_blank">Les cookies et les dangers</a> et <a
        href="https://forum.malekal.com/viewtopic.php?t=43115&start="
        target="_blank">Cookies et vols de sessions</a></li>
    </ul>

    <div class="note">QRcode: <button id="qrcode_url_show">URL du site</button>&nbsp;&nbsp;&nbsp;<button id="qrcode_vcard_show">Ma vCard</button>&nbsp;&nbsp;&nbsp;<button id="qrcode_hide" style="display:none;">Cacher</button></div>
</div>
    <div class="note" id="qrcode_url" style="display: none;">URL du site :<br /><img src="../commun/qr_code_url.png" alt="Quick Response code URL" /><br />QRcode generator : <a href="http://zxing.appspot.com/generator/">http://zxing.appspot.com/generator/</a></div>
    <div class="note" id="qrcode_vcard" style="display: none;">My vCard (maj au 19/07/2015) :<br /><img src="../commun/qr_code_vcard.png" alt="Quick Response code - vCard" /><br />QRcode generator : <a href="http://zxing.appspot.com/generator/">http://zxing.appspot.com/generator/</a></div>


<?php include ("../commun/inc-pied.php"); ?>

</body>
</html>
