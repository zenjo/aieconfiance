
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="fr">
<head>
<?php include ("../commun/inc-meta.php"); ?>
<title>Aie confiance! Résumé licence</title>
</head>
<body>

<div>
Copyright (c)  2012  Banlieues asbl, Cassiopea asbl.<br>
This program is free software: you can redistribute it and/or modify it under the terms of the <a href="http://www.gnu.org/licenses/gpl.txt" target="_blank">GNU General Public License</a> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.<br><br>

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.<br><br>

You should have received a copy of the GNU General Public License along with this program, see the GNU_General_Public_License directory.  If not, see <a href="http://www.gnu.org/licenses/gpl.txt" target="_blank">http://www.gnu.org/licenses/gpl.txt</a>.
</div>

</body>
</html>