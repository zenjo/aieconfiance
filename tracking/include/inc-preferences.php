<?php

// Preferences de navigation
$prefexiste = false;
if (isset($_COOKIE["preferences"])) {
	$visitepages = $_COOKIE["preferences"];
	$visitepagesarray = explode(",", $visitepages);
	$maxpref = max($visitepagesarray); $keypagepref = -1;
	foreach ($visitepagesarray as $key => $val) {if ($val == $maxpref) {$keypagepref = $key;}}
	// pagebleue = 0
	// pagerouge = 1
	// pageverte = 2
	if ($keypagepref >= 0) {
		$prefexiste = true;
		switch($keypagepref) {
			case 0: $pagepref = "blue"; $pageprefcolor = "white"; break;
			case 1: $pagepref = "red"; $pageprefcolor = "white"; break;
			case 2: $pagepref = "green"; $pageprefcolor = "black"; break;
			}
		}
	$nbvisitepage0=$visitepagesarray[0];
	$nbvisitepage1=$visitepagesarray[1];
	$nbvisitepage2=$visitepagesarray[2];

	$nbvisitepage_string="<b>Préférences de navigation:</b> <a href='pagebleue.php' style='color:yellow'>Page bleue</a>: ".$nbvisitepage0.", <a href='pagerouge.php' style='color:yellow'>Page rouge</a>: ".$nbvisitepage1.", <a href='pageverte.php' style='color:yellow'>Page verte</a>: ".$nbvisitepage2."<br>Nous avons adapté la banniere publicitaire à vos préférences.<br><span style='font-size:75%'>La publicité verte est plus chère que la rouge qui est plus chère que la bleue. Donc, en cas d'égalité de préférences, c'est d'abord la verte, puis la rouge, puis la bleue qui sera affichée.</span>";
	}
else {
	$visitepages = "0,0,0";
	$nbvisitepage_string="Vous n'avez pas encore de préférences de navigation";
	}
?>
