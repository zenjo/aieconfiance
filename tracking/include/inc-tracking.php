<?php

function diff_heure($ts) {
   $jo=24*60*60;$ho=60*60;$mo=60;
   $j=0;$h=0;$m=0;$s=0;
   if ($ts >= $jo) {$j = floor($ts/$jo); $ts = $ts % ($j*$jo);}
   if ($ts >= $ho) {$h = floor($ts/$ho); $ts = $ts % ($h*$ho);} 
   if ($ts >= $mo) {$m = floor($ts/$mo); $s = $ts % ($m*$mo);} else {$s = $ts;}
   
   $z = "0";
   $h2 = ($h < 10)?$z.$h:$h;
   $m2 = ($m < 10)?$z.$m:$m;
   $s2 = ($s < 10)?$z.$s:$s;
   $texte = $j."j. ".$h2."h. ".$m2."m. ".$s2."s. ";
   return $texte;
} 

// Referer
if (isset($_SERVER['HTTP_REFERER'])) {$refer = $_SERVER['HTTP_REFERER']; $referbool=true;}
   else {$refer = "Vous venez de cette page"; $referbool=false;}

// temps ecoule
$heurecourante = time();
if (isset($_COOKIE['heurecourante'])) {
   $heureprecedente=$_COOKIE['heurecourante'];
   $tempsecoule=diff_heure($heurecourante-$heureprecedente);
//   $tempsecoule=diff_heure(47*60*60+60*3+50);
   $tempsecoule_string="Temps &eacute;coul&eacute; depuis votre derni&egrave;re visite : ".$tempsecoule;
   } 
else {
   $tempsecoule_string="C'est la premi&egrave;re fois que vous nous visitez.";
   }
setcookie("heurecourante",$heurecourante,time()+60*60*24*365*25);

// changement de nom, prenom, email
if (isset($_POST['nom_form']) && isset($_POST['coords_submit']) && $_POST['nom_form'] != "") 
	{$nom=$_POST['nom_form'];setcookie("nom",$nom,time()+60*60*24*365*25);}
if (isset($_POST['prenom_form']) && isset($_POST['coords_submit']) && $_POST['prenom_form'] != "") 
	{$prenom=$_POST['prenom_form'];setcookie("prenom",$prenom,time()+60*60*24*365*25);}
if (isset($_POST['email_form']) && isset($_POST['coords_submit']) && $_POST['email_form'] != "") 
	{$email=$_POST['email_form'];setcookie("email",$email,time()+60*60*24*365*25);}

// cretaion ou maintien de l'id
if (isset($_COOKIE['id'])) {
   $id=$_COOKIE['id'];
   if ($ancien_nom == $nom AND $ancien_prenom == $prenom AND $ancien_email == $email) 
   		{$id_string="Revoila notre cher utilisateur n&deg; ".$id." ;-)";}
   		else {$id_string="Notre cher utilisateur n&deg; ".$id." vient de changer soit son nom: ".$ancien_nom." pour ".$nom.", soit son prénom: ".$ancien_prenom." pour ".$prenom.", soit son email: ".$ancien_email." pour ".$email.". Faudra penser à enregistrer cela aussi.";}
   } 
else {
   $id=mt_rand(1000000,9999999); 
   setcookie("id",$id,time()+60*60*24*365*25); 
   $id_string="Vous &ecirc;tes un nouvel utilisateur. Votre id est fix&eacute;e &agrave; ".$id;
   }

// Nb visites
if (isset($_COOKIE['nombrevisites'])) {
   $nbvisite=$_COOKIE['nombrevisites']+1;
   $nbvisite_string="C'est votre ".$nbvisite."&egrave;me visite de cette page.";
   } 
else {
   $nbvisite=1; 
   $nbvisite_string="C'est votre 1&egrave;re visite de cette page.";
   }
setcookie("nombrevisites",$nbvisite,time()+60*60*24*365*25);


?>
