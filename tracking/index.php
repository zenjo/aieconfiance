<!doctype html>
<html lang="fr">
<head>
<?php include ("../commun/inc-meta.php"); ?>
<title>Simulation de site "gratuit"</title>
<link rel="stylesheet" type="text/css" href="tracking.css">
</head>
<body>
<?php include "../commun/inc-entete.php"; ?>
<div class="encart"><h2><a href='../accueil' title='Accueil Aie confiance!'>Aie confiance !</a></h2><p><img src="../commun/big-brother-reduit.jpg"></p></div>
<h1>Simulation de site "gratuit"</h1>
<div><a href="tracking.php">Commencer ou pousuivre la simulation</a></div>
<div style="border: 1px solid grey;padding 10px">
    <h2>Règles du jeu</h2>
    <div>
        <p>Ce site tente de donner une idée du mécanisme réel de sites dit "gratuits". Aucune information (autre que les log habituels) n'est enregistrée côté serveur par le site (il n'y a pas de base de données associée au site). Les informations retenues sont des cookies (non dangereux) installés sur votre ordinateur et que vous pouvez effacer quand vous voulez. Bien entendu, vous pouvez douter de ce qui précède, c'est un bon réflexe. Dans les données "obligatoires", vous pouvez tout à fait entrer ... n'importe quoi ;).</p>
    </div>
    <div>
        <p>Les exemples sont simples, et juste illustratifs. Dans la réalité, la récolte et le traitement des informations seraient certainement plus sophistiqués, et les données enregistrées dans une base.</p>
    </div>
    <div class="infocachees">
        Dans ce format blanc sur noir, vous trouverez des informations qui peuvent être collectées et que vous ne voyez en général pas
    </div>
</div>

<?php include ("../commun/inc-pied.php"); ?>

</body>
</html>
