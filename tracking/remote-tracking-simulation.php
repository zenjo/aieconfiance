<?php
// Définit le contenu de l'en-tête - dans ce cas, image/jpeg
//header('Content-Type: image/jpeg');
header('Content-Type: image/png');

// Création d'une image vide et ajout d'un texte
//$im = imagecreatetruecolor(400, 25);
$im = imagecreate(400, 25);
$background = imagecolorallocate( $im, 0, 0, 255 );
$text_color = imagecolorallocate($im, 255, 255, 0);
imagestring($im, 4, 5, 5,  'Je suis une image espion de 400x25 pixels.', $text_color);

// Affichage de l'image
//imagejpeg($im);
imagepng($im);

// Libération de la mémoire
imagedestroy($im);

// Je sauve dans un fichier "plat", dans la réalité, ce serait dans une base de données
$data = "id : ".$_GET["id"]."\nip : ".$_GET["ip"]."\nnom : ".$_GET["nom"]."\nprenom : ".$_GET["prenom"]."\nemail : ".$_GET["email"]."\nsite : ".$_GET["site"]."\npreferences : ".$_GET["pref"]."\nnb visites : ".$_GET["visites"];
file_put_contents("donnees_collectees.txt", $data);
?>
