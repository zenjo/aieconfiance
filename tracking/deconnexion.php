<?php
If (isset($_POST['submit_dec'])) {
    setcookie ("nom", "", time() - 3600);
    setcookie ("prenom", "", time() - 3600);
    setcookie ("email", "", time() - 3600);
}

include ("include/inc-coords.php");
include ("include/inc-tracking.php");
include ("include/inc-preferences.php");

?>
<!doctype html>
<html lang="fr">
<head>
<?php include ("../commun/inc-meta.php"); ?>
<title>Au revoir ...</title>
<link rel="stylesheet" type="text/css" href="tracking.css">
</head>
<body>
<?php include ("../commun/inc-entete.php"); ?>
<?php include ("include/inc-navigation.php"); ?>
<div>
<?php
    echo "<h3>Au revoir et à bientôt, cher ami ".$prenom." ".$nom." - ".$email." !</h3>";
    echo "<p>Vos nom [".$nom."], prénom [".$prenom."] et email [".$email."] ont été effacés de notre base de données.<br />";
?>
<p>Pour bien vérifier l'effacement des nom, prénom et email, <a href="deconnexion.php">rechargez la page</a>.</p>
</div>

<div class="infocachees">
<?php
echo "<p><b>Mais pas le reste! :</b></p>";

if (isset($_COOKIE['id']))
    {echo "- Nom: id | Valeur: ".$id."<br />";}
if (isset($_COOKIE['heurecourante']))
    {echo "- Nom: heurecourante | Valeur: ".$heurecourante."<br />";}
if (isset($_COOKIE['nombrevisites']))
    {echo "- Nom: nombrevisites | Valeur: ".$nbvisite."<br />";}
if (isset($_COOKIE['preferences']))
    {echo "- Nom: preferences | Valeur: ".$visitepages."<br />";}


?>
</div>


<?php include ("../commun/inc-pied.php"); ?>

</body>
</html>
