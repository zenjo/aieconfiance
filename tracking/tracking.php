<?php
include ("include/inc-coords.php");
include ("include/inc-tracking.php");
include ("include/inc-preferences.php");

/**
 * Récupérer la véritable adresse IP d'un visiteur
 */
function get_ip() {
// IP si internet partagé
    if (isset($_SERVER['HTTP_CLIENT_IP'])) {
    return $_SERVER['HTTP_CLIENT_IP'];
    }
// IP derrière un proxy
    elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    return $_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    // Sinon : IP normale
    else {
    return (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '');
    }
}

?>
<!doctype html>
<html lang="fr">
<head>
<?php include ("../commun/inc-meta.php"); ?>
<title>Simulation de site "gratuit": les traces</title>
</head>
<style type="text/css">
</style>
<body>

<?php include ("../commun/inc-entete.php"); ?>

<h1>Simulation de site "gratuit": les traces</h1>
<div class="infocachees">Dans ce format blanc sur noir, des informations qui peuvent être collectées et que vous ne voyez en général pas</div>

<?php include ("include/inc-navigation.php"); ?>

<?php
$infocol = 1;
if (isset($_GET['infocollected'])) {$infocol = $_GET['infocollected'];}
if ($infocol == "0")
    {echo "<div style='border: 1px solid black; background-color: red; color: yellow; font-size:120%;padding:10px;'>ATTENTION! Vous devez compléter les informations obligatoires pour accéder à nos servcies gratuits</div>";}
?>

<?php
// Banniere publicitaire bidon, si préférences
if ($prefexiste) {
    echo "<div style='background-color:".$pagepref.";color:".$pageprefcolor.";border: 2px solid black;padding:20px;margin:15px;fontweight:bold;font-size:200%;text-align:center;height:60px;'>Bannière publicitaire <i>".$pagepref."</i> adaptée à vos préférences<div style='font-size:50%;'>Le site \"gratuit\" est en fait financé par une firme qui incluera dans le prix de ses produits, cette publicité.<br />En finale, le consommateur, vous et moi, paie le site dit \"gratuit\".</div></div>";
    }
else {
        echo "<div style='border: 2px solid black;padding:20px;margin:15px;fontweight:bold;font-size:200%;text-align:center;min-height:60px;'>Bannière publicitaire quelconque.</div>";
    }
?>

<!----------------------------+
|* Préférences de navigation *|
+----------------------------->
<div class="infocachees">
<?php echo $nbvisitepage_string."<br />"; ?>
</div>

<?php

if ($prefexiste) {
    echo '<div style="margin:auto;margin-bottom:15px;width:100%;text-align:center;"><div  style="float:left;width:60%"><img src="images/2010_09_07_schema_fausse_gratuite.png" width="562" height="337" alt="" /></div><div  style="float:left;width:30%;vertical-align:top;"><h1>Aie confiance!</h1><!-- <img src="../commun/kaa-aie-confiance.jpg"> --><img src="../commun/big-brother-reduit.jpg"></div></div><br style="clear:both;">';
    }

if ($prenom == "" && $nom == "")
    {echo "<h3>Bonjour Ami Anonyme</h3>";}
else
    {echo "<h3>Bonjour Ami ".$prenom." ".$nom."</h3>";}
?>

<div class="infocachees">
<?php echo "Email: ".$email."<br />"; ?>
</div>

<!--------------------------+
|* Navigateur, IP, langues *|
+--------------------------->
<h3>Vous, votre navigation, votre navigateur, vos préférences de langues</h3>

<?php if ($nom != "" AND $prenom != "" AND $email != "") { ?>
    <div style="border: 1px solid black; margin: 10px; paddind: 10px;"><b>Après nous avoir transmis les informations ci-dessus, vous bénéficiez maintenant de nos services gratuits, page <a href="pagebleue.php"><span style="color:blue;">bleue</span></a>, <a href="pagerouge.php"><span style="color:red;">rouge</span></a> et <a href="pageverte.php"><span style="color:green;">verte</span></a></b></div>
<div>
<form action="deconnexion.php" method="POST">
    <input type="submit" value="Déconnexion" name="submit_dec">
</form>
</div>

<?php } else { ?>
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
    <div><b>Ces champs sont obligatoires pour bénéficier de nos services gratuits, page <a href="pagebleue.php"><span style="color:blue;">bleue</span></a>, <a href="pagerouge.php"><span style="color:red;">rouge</span></a> et <a href="pageverte.php"><span style="color:green;">verte</span></a>:</b></div>
    Votre prénom (obligatoire): <input type="text" name="prenom_form" value="<?php echo $prenom; ?>" onClick="this.select();"><br />
    Votre nom (obligatoire): <input type="text" name="nom_form" value="<?php echo $nom; ?>" onClick="this.select();"><br />
    Votre email (obligatoire): <input type="text" name="email_form" value="<?php echo $email; ?>" onClick="this.select();"><br />
    <input name="coords_submit" value="Envoyer" type="submit">
    </form>
<?php } ?>

<div class="infocachees">
<?php
echo $id_string."<br />";
echo "Aujourd'hui : ".date("D d M Y, H\h. i\m. s\s.",$heurecourante)."<br />";
echo "Derni&egrave;re visite : ".date("D d M Y, H\h. i\m. s\s.",$heureprecedente)."<br />";
echo $nbvisite_string." Ce type de visite s'appelle en général un \"hit\". Le calcul du nombre de visites sur un site (comprenant par défintion plusieurs pages) est un calcul complexe, faisant référence à diverses méthodes et une certaine part de subjectivité.<br />";
echo $tempsecoule_string."<br />";
echo "Vous venez de : ".$refer."<hr>";

echo "<u>Cookies plac&eacute;s sur votre ordinateur :</u><br />";
if (isset($_COOKIE['id']))
    {echo "- Nom: id | Valeur: ".$id."<br />";}
if (isset($_COOKIE['heurecourante']))
    {echo "- Nom: heurecourante | Valeur: ".$heurecourante."<br />";}
if (isset($_COOKIE['nom']))
    {echo "- Nom: nom | Valeur: ".$nom."<br />";}
if (isset($_COOKIE['prenom']))
    {echo "- Nom: prenom | Valeur: ".$prenom."<br />";}
if (isset($_COOKIE['email']))
    {echo "- Nom: email | Valeur: ".$email."<br />";}
if (isset($_COOKIE['nombrevisites']))
    {echo "- Nom: nombrevisites | Valeur: ".$nbvisite."<br />";}
if (isset($_COOKIE['preferences']))
    {echo "- Nom: preferences | Valeur: ".$visitepages."<br />";}

echo "<hr>Vous êtes sur le serveur IP - host - virtual host : ".$_SERVER['SERVER_ADDR']." - ".gethostbyaddr($_SERVER['SERVER_ADDR'])." - ".$_SERVER['SERVER_NAME']."<br />";
echo "Vous venez du serveur IP - host : ".$_SERVER['REMOTE_ADDR']." - ".gethostbyaddr($_SERVER['REMOTE_ADDR'])."<br />";
echo "Vous venez de la page : ".$_SERVER['HTTP_REFERER']."<br />";
echo "Votre IP actuelle: ".get_ip()."<br />";
echo "Vous ex&eacute;cutez le script : ".$_SERVER['PHP_SELF']."<hr>";

echo "<u>Votre syst&egrave;me :</u><br />";
?>

<script type="text/javascript">
<!--
   document.write("Système d'exploitation : "+navigator.platform+"<br />\n");
 // -->
</script>

<?php
echo "Votre navigateur : ".$_SERVER['HTTP_USER_AGENT']."<br />";
?>

<script type="text/javascript">
<!--
   document.write("Largeur de votre &eacute;cran : "+screen.width+"px<br />\n");
   document.write("Hauteur de votre &eacute;cran : "+screen.height+"px<br />\n");
   document.write("Profondeur des couleurs : "+screen.colorDepth+"<br />\n");
   document.write("Intensit&eacute; des couleurs : "+screen.pixelDepth+"<br />\n");
   document.write("Nombre d'&eacute;l&eacute;ments de l'historique pour cette page : "+history.length+"<br />\n");
   document.write("Vous êtes en ligne maintenant : "+navigator.onLine+"<br />\n");
   document.write("Les cookies sont activés : "+navigator.cookieEnabled+"<br />\n");
   document.write("Java (DANGER!) est activé : "+navigator.javaEnabled()+"<br />\n");
 // -->
</script>

<?php
echo "<br /><u>Pr&eacute;f&eacute;rences de langue de votre navigateur (par ordre de priorit&eacute;) :</u> <br />";
$langpref = ""; $langprefpref = ""; $lang_browser = ""; $lang_browser_array_prov = array(); $lang_browser_array = array();
$lang_browser = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
$lang_browser_array_prov = explode(",",$lang_browser);
foreach ($lang_browser_array_prov as $val) {
   if (preg_match("/q=/m",$val)) {$langpref = str_replace(";q=", " - ", $val);} else {$langpref = $val." - 1";$langpref1 = $val;}
   echo $langpref."<br />";
   }
?>
</div>

<!------------------+
|* Géolocalisation *|
+------------------->
<h3>La géolocalisation</h3>

<!-- <div class="note">Apparemment, Google ne supporte plus la géolocalisation gratuite. Voyez <a href="https://developers.google.com/maps/documentation/geocoding/usage-and-billing?hl=fr">A new pay-as-you-go pricing model is now in effect for Google Maps Platform APIs</a>. Je cherche une autre solution de géolocalisation pour cette démonstration</div> -->

<div>
<p>La géolocalisation utilise la <a href="https://www.w3.org/TR/geolocation-API/">Geolocation API Specification</a> (html5) du W3C.<br />
Vous trouverez ici un bon tutoriel sur la <a href="https://www.alsacreations.com/tuto/lire/926-geolocalisation-geolocation-html5.html">géolocalisation</a>.</p>
</div>

<div class="infocachees">
<u>Géolocalisation</u>

<!-- Un élément HTML pour recueillir l’affichage -->
<div id="infoposition"></div>

</div>

<div>
Utilisons maintenant l'<a href="https://developers.google.com/maps/documentation/javascript/geocoding">API javascript Google maps</a> pour montrer les résultats:
</div>
<div class="note">
16/06/2018 : le nombre de requetes est limité à 2500 / jours, 50 / secondes <a href="https://developers.google.com/maps/documentation/geocoding/usage-limits?hl=fr">https://developers.google.com/maps/documentation/geocoding/usage-limits?hl=fr</a>.
</div>


<div id="map" style="width:640px;height:480px"></div>


<!-- Les scripts de géolocalisation -->
<!-- Chargement de l'API Google maps -->

<?php include ("api/k"); ?>

<script>

// Position par défaut
var centerpos = new google.maps.LatLng(48.579400,7.7519);

// Ansi que des options pour la carte, centrée sur latlng
var optionsGmaps = {
    center:centerpos,
    navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    zoom: 15
};

// Initialisation de la carte avec les options
var map = new google.maps.Map(document.getElementById("map"), optionsGmaps);

if(navigator.geolocation) {

    // Fonction de callback en cas de succès
    function affichePosition(position) {

        var infopos = "Position déterminée : <br />";
        infopos += "Latitude : "+position.coords.latitude +"<br />";
        infopos += "Longitude: "+position.coords.longitude+"<br />";
        infopos += "Altitude : "+position.coords.altitude +"<br />";
        document.getElementById("infoposition").innerHTML = infopos;

        // On instancie un nouvel objet LatLng pour Google Maps
        var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

        // Ajout d'un marqueur à la position trouvée
        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            title:"Vous êtes ici"
        });

        map.panTo(latlng);

    }

    // Fonction de callback en cas d’erreur
    function erreurPosition(error) {
        var info = "Erreur lors de la géolocalisation : ";
        switch(error.code) {
        case error.TIMEOUT:
            info += "Timeout !";
        break;
        case error.PERMISSION_DENIED:
            info += "Vous n’avez pas donné la permission";
        break;
        case error.POSITION_UNAVAILABLE:
            info += "La position n’a pu être déterminée";
        break;
        case error.UNKNOWN_ERROR:
            info += "Erreur inconnue";
        break;
        }
        document.getElementById("infoposition").innerHTML = info;
    }

    navigator.geolocation.getCurrentPosition(affichePosition,erreurPosition);

} else {

    alert("Ce navigateur ne supporte pas la géolocalisation");

}

</script>


<div>


<?php
echo "<div style='background-color:".$pagepref.";color:".$pageprefcolor.";border: 2px solid black;padding:20px;margin:15px;fontweight:bold;font-size:200%;text-align:center'>";
echo "Ici, par exemple, une publicité <i>".$pagepref."</i> en ".$langpref1." (langue préférée)</div>";
?>
</div>

<!----------------+
|* La web beacon *|
+----------------->
<h3>La web beacon</h3>
<div>
<p>Le propriétaire de ce site souhaite aussi que son agence publicitaire suive certaines données à distance, et à travers votre navigation dans ce site et aussi dans tous ceux de son réseau. Nous allons donc permettre à l'agence d'installer un outil de tracking pour qu'elle puisse réaliser cela.</p>
</div>

<div class="infocachees">
<u>La web beacon</u><br />
<?php
$url_espion = "http://aieconfiance.cassiopea.org/remote-tracking-simulation-externe.php?id=".$id."&site=".$_SERVER['SERVER_NAME']."&ip=".get_ip()."&nom=".$nom."&prenom=".$prenom."&email=".$email."&pref=".$visitepages."&visites=".$nbvisite;
?>

<p><img src="<?php echo $url_espion; ?>" width=400 height=25 border=1></p>
<p>Dans la réalité, je mesurerais 1x1 pixel, serais transparente et n'aurais pas de bordure; bref, vous ne me verriez pas.</p>
<p>Mon code html est le suivant &lt;img src="<?php echo $url_espion; ?>" width=400 height=25 border=1&gt;</p>
<p>Dans la réalité il y aurait "width=1 height=1 border=0" (largeur, hauteur = 1 pixel, pas de bordure), mais le problème n'est pas là. Il est dans le src="https://...".</p>
<p>Comment ça marche ?</p>
</div>

<h4>Explication de l'url</h4>

<img src="images/decompose_url.png" alt="Explication de l'url">

<div>

Le script est ici&nbsp;: <?php echo '<a href='.$url_espion.' target="_blank">remote-tracking-simulation-externe.php</a>' ?>, et voici son code&nbsp;:
<div  class="infocachees">
<pre>
&lt;?php
// Définit le contenu de l'en-tête - dans ce cas, image/jpeg
//header('Content-Type: image/jpeg');
header('Content-Type: image/png');

// Création d'une image vide et ajout d'un texte
//$im = imagecreatetruecolor(400, 25);
$im = imagecreate(400, 25);
$background = imagecolorallocate( $im, 0, 0, 255 );
$text_color = imagecolorallocate($im, 255, 255, 0);
imagestring($im, 4, 5, 5,  'Je suis une image espion de 400x25 pixels.', $text_color);

// Affichage de l'image
//imagejpeg($im);
imagepng($im);

// Libération de la mémoire
imagedestroy($im);

// Je sauve dans un fichier "plat", dans la réalité, ce serait dans une base de données
$data = "id : ".$_GET["id"]."\nip : ".$_GET["ip"]."\nnom : ".$_GET["nom"]."\nprenom : ".$_GET["prenom"]."\nemail : ".$_GET["email"]."\nsite : ".$_GET["site"]."\npreferences : ".$_GET["pref"]."\nnb visites : ".$_GET["visites"];
file_put_contents("donnees_collectees_externe.txt", $data);
?&gt;
</pre>
</div>

<u>Les faits:</u>
<ul>
<li>La source de "l'image" (src="http...") fait appel à un serveur distant. </li>
<li>Elle ne fait pas appel à une image (extension jpeg, png, gif), mais à un script php (remote-tracking-simulation.php)</li>
<li>Elle passe des paramètres (ceux que nous avons choisi pour le suivi de notre agence) qui seront envoyé au script php distant. Voici ces paramètres:
    <ul>
    <li>votre id (<?php echo $id; ?>), votre ip (<?php echo get_ip(); ?>, permettre une certaine identification)</li>
    <li>vos nom, prénom et email (<?php echo $nom.", ".$prenom.", ".$email; ?>, en vue d'une campagne publicitaire)</li>
    <li>le site où vous êtes (<?php echo $_SERVER['SERVER_NAME']; ?>, on cherche à vous traquer sur un réseau de sites)</li>
    <li>vos préférences de navigation pour ce site (<?php echo $visitepages; ?>, visistes page bleue, rouge et verte)</li>
    <li>le nombre de vos visites (<?php echo $nbvisite; ?>)</li>
    </ul>
</li>
</ul>

<u>Le mécanisme</u>
<p>Lorsque votre navigateur affiche la page où se trouve l'image, il cherche sa source, dans src="https://..." et envoie ainsi une requête au script remote-tracking-simulation.php sur un serveur distant traqueur, avec des paramètres.</p>
<p>Le script remote-tracking-simulation.php va faire deux choses:</p>
<ol>
<li>il va renvoyer à cette page une image qu'il a fabriquée (je ne vous fais pas cliquer sur l'url du script, voyez plus loin pourquoi).</li>
<Li>il va récupérer les paramètres reçu de la requête et les insérer dans une base de données en vue d'un traitement ultérieur (comparaison avec les autres sites du réseau, revente, data mining, …). Tout est en place également pour d'éventuelles (et probables) campagnes de publicité personnalisée et automatisée.</li>
</ol>

<p><img src="images/requete_web_beacon.jpg"  alt="Le mécanisme de requête de la web beacon"></p>

<p><i>"Big brother is watching you, and Terminator is coming soon !"</i></p>
<p>Rassurez vous, cette démo ne collecte aucune donnée ! Son script remote-tracking-simulation.php se contente de renvoyer la pseudo image espion, et - preuve oblige - enregistre la dernière requête reçue dans un fichier plat (donnees_collectees_externe.txt à ouvrir par ftp). Ce fichier ne collectionne rien et est renouvelé à chaque fois</p>
<p>Dans la réalité, le script remote-tracking-simulation.php aurait enregistré les données passées en paramètres par l'url de la web beacon dans une base de données en vue de traitement.</p>
<p>Voyez le schéma ci-dessous qui décrit de manière simplifiée le mécanisme (dans la réalité, ce serait beaucoup plus sophistiqué). Dans ce cas de figure, la seule intervention humaine est celle d'un visiteur nommé Jean Sonnom; tout le reste est devenu automatique. La web beacon ne transmet que les nom, prénom, email, site visité et préférences de navigation pour ce site.</p>
<p><i><b>Note : Mettre à jour pour le RGPD, mais fondammentalement, ça ne change pas grand chose au ciblage.</b></i></p>
<p><img src="images/web-beacon-network.jpg" width=936 height=1102 alt="Description de l'utilité d'une web beacon dans un réseau de site" style="margin:auto;"></p>
<p>Si vous comprenez ce qui précède, vous avez compris le principe technique de la web beacon et son utilité. Reste les enjeux ...</p>
</div>

<!--
<h3>Web beacon externe</h3>
<div>
<p>Pour les exercices pratiques ...</p>
</div>


<div class="infocachees">
<u>La web beacon externe</u><br />
-->
<?php
$url_espion_externe = "http://aieconfiance.cassiopea.org/remote-tracking-simulation-externe.php?id=".$id."&site=".$_SERVER['SERVER_NAME']."&ip=".get_ip()."&nom=".$nom."&prenom=".$prenom."&email=".$email."&pref=".$visitepages."&visites=".$nbvisite;
?>

<p><img src="<?php echo $url_espion_externe; ?>" width=1 height=1 border=0></p>
<!--
<p>Dans la réalité, je mesurerais 1x1 pixel, serais transparente et n'aurais pas de bordure; bref, vous ne me verriez pas.</p>
<p>Mon code html est le suivant &lt;img src="<?php echo $url_espion_externe; ?>" width=400 height=25 border=1&gt;</p>
<p>Dans la réalité il y aurait "width=1 height=1 border=0" (largeur, hauteur = 1 pixel, pas de bordure).</p>
</div>
<div><p>Et nous pouvons voir les données collectées dans <a href="http://aieconfiance.cassiopea.org/donnees_collectees_externe.txt">http://aieconfiance.cassiopea.org/donnees_collectees_externe.txt</a></p></div>
-->

<h3>Et, en avant-première, nous avons le plaisir de vous présenter</h3>
<div class="note">- sonnez hautbois, résonnez musettes - une authentique webbeacon dans un message reçu sur une liste mailchimp, au 06/04/2018&nbsp;:</div>
<pre>
   X-Mailer: MailChimp Mailer - **CID2ce2562462649b0eb381**
   ...
   &lt;img src=3D"https://&lt;nom_propriétaire_ liste&gt;.us10.list-manage.com/track/open.ph=p?u=3Df6f87ad827e354b63d3d02fec&id=3Dbd32aafa17&e=3D649b0eb381" height=3D="1" width=3D"1"&gt;
</pre>
<div class="note">Vous noterez les dimensions de l'image&nbsp;: height="1" width="1", soit 1x1 pixel.</div>

<!----------------------+
|* plugins et fichiers *|
+----------------------->
<h3>Les plugins et types de fichiers reconnus par votre navigateur</h3>

<div class="infocachees">
<script type="text/javascript">
<!--
   document.writeln("<br /><u>Plugin(s) de votre navigateur install&eacute;(s) :</u><br /><table border=\"0\">");
   for(var i=0; i<navigator.plugins.length; i++){
      document.writeln("<tr>");
      document.writeln("<td>" + navigator.plugins[i].name + "<\/td>");
      document.writeln("<td>" + navigator.plugins[i].description + "<\/td>");
      document.writeln("<td>" + navigator.plugins[i].filename + "<\/td>");
      document.writeln("<\/tr>");
      }
   document.writeln("<\/table><br />");

   document.writeln("<br /><u>Types de fichiers que votre navigateur connait :</u><br /><table border=\"0\">");
   for(var i=0; i<navigator.mimeTypes.length; i++){
      document.writeln("<tr>");
      document.writeln("<td>" + navigator.mimeTypes[i].type + "<\/td>");
      document.writeln("<td>" + navigator.mimeTypes[i].suffixes + "<\/td>");
      document.writeln("<td>" + navigator.mimeTypes[i].description + "<\/td>");
      if (navigator.mimeTypes[i].enabledPlugin)
         document.writeln("<td>" + navigator.mimeTypes[i].enabledPlugin + "<\/td>");
      else document.writeln("<td>Pas de plugiciel<\/td>");
      document.writeln("<\/tr>");
   }
   document.writeln("<\/table>");

 // -->
</script>
</div>

<?php include ("../commun/inc-pied.php"); ?>

</body>
</html>
