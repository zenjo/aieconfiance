<meta HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=UTF-8">
<meta name="keywords" lang="fr" content="Internet, pedagogie, Aie confiance, tracking, collecte d'adresses email, spambot">
<meta name="generator" content="Komodo">
<meta name="author" CONTENT="Banlieues & Cassiopea">
<meta name="description" content="Petits programmes Internet pédagogiques à propos de choses cachées souvent ignorées, telles que collecte d'informations personnelles, tracking, collecte d'adresses mail, etc. ">
<meta name="created" CONTENT="2012-09-25">
<meta name="robots" content="follow">

<!-- Dublin Core Metadata -->
<meta name="DC.Title" content="Aie confiance!">
<meta name="DC.Creator" content="Banlieues & Cassiopea">
<meta name="DC.Subject" content="Programmes Internet pédagogiques">
<meta name="DC.Description"
      content="Petits programmes Internet pédagogiques à propos de choses cachées souvent ignorées, telles que collecte d'informations personnelles, tracking, collecte d'adresses mail, etc. ">
<meta name="DC.Publisher" content="Banlieues & Cassiopea">
<meta name="DC.Contributor" content="Banlieues & Cassiopea">
<meta name="DC.Date" content="2012-09-25">
<meta name="DC.Type" content="texte">
<meta name="DC.Format" content="text/html">
<meta name="DC.Identifier" content="">
<meta name="DC.Source" content="Banlieues & Cassiopea">
<meta name="DC.Language" content="fr">
<meta name="DC.Coverage" content="Belgique">
<meta name="DC.Rights" content="Tous les droits sont chez les auteurs">
<link rel="shortcut icon" href="../commun/kaa.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="../commun/aieconfiance.css">

<script type="text/javascript" src="../commun/js//jquery-1.7.2.js"></script>
<script type="text/javascript" src="../commun/js/jquery.flexibleArea.js"></script>
<script type="text/javascript" src="../commun/js/jquery-fonctions-communes.js"></script>

<script type="text/javascript">

function license() {
	alert('Copyright (c)  2012  Banlieues asbl, Cassiopea asbl.\nPermission is granted to copy, distribute and/or modify this document\nunder the terms of the <a href="http://www.gnu.org/licenses/fdl-1.3-standalone.html" target="_blank">GNU Free Documentation License, Version 1.3</a>\nor any later version published by the Free Software Foundation;\nwith no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.\nA copy of the license is included in the section entitled "GNU_Free_Documentation_License".');
}

</script>

