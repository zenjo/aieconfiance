

$(document).ready(function(){
	
  $("a#licence").click(function(){
    $("div.licence").toggle();
  });


    /********************************/
    /* Afficher / cacher les QRcode */
    /********************************/
    $("button#qrcode_url_show").click(function(){
		$("div#qrcode_vcard").hide();
        $("div#qrcode_url").show();
        $("button#qrcode_hide").show();
    });

    $("button#qrcode_vcard_show").click(function(){
        $("div#qrcode_url").hide();
        $("div#qrcode_vcard").show();
        $("button#qrcode_hide").show();
    });

    $("button#qrcode_hide").click(function(){
        $("div#qrcode_url").hide();
        $("div#qrcode_vcard").hide();
        $("button#qrcode_hide").hide();
    });

});
