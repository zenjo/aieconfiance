
<div class="licence starthidden">
Copyright (c)  2012  Cassiopea asbl.<br>
This program is free software: you can redistribute it and/or modify it under the terms of the <a href="http://www.gnu.org/licenses/gpl.txt" target="_blank">GNU General Public License</a> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.<br><br>

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.<br><br>

You should have received a copy of the GNU General Public License along with this program, see the GNU_General_Public_License directory.  If not, see <a href="http://www.gnu.org/licenses/gpl.txt" target="_blank">http://www.gnu.org/licenses/gpl.txt</a>.

</div>

<div class="pied">

    <p>
        Le contenu de ce site est sous licence <a onclick="javascript:return false;" href="../accueil/resume_licence.php" id="licence">GNU GPL</a>
     - Code source : <a href="https://framagit.org/zenjo/aieconfiance" ><img src="../commun/framagit_logo.png" width="28" height="28" alt="Logo Framagit"></a>
     </p>
</div>

<!-- Piwik -->
<script type="text/javascript">
  var _paq = _paq || [];
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="//web.sebille.name/piwik/";
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', 9]);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<noscript><p><img src="//web.sebille.name/piwik/piwik.php?idsite=9" style="border:0;" alt="" /></p></noscript>
<!-- End Piwik Code -->

