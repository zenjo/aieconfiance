<?php
// afficher le source
$show_source = true;
if(isset($_GET['show_source']))
    {$show_source = $_GET['show_source'];if ($show_source == "true") {echo highlight_file(__FILE__, 1);exit;}}

// traiter captcha et cokkie
$robot_possible = true; $nom_cookie_robot = "EtreHumain";
if (isset($_POST["verif"]) and ($_POST["captcha"] == ($_POST["un"] + $_POST["deux"]))) {
    $robot_possible = false;
    setcookie($nom_cookie_robot, "Oui", time()+(3600*24*365));
}
if ($_COOKIE[$nom_cookie_robot] == "Oui") {$robot_possible = false;} else {$un = rand(1,3);$deux = rand(1,3);}

//$texte = "Bonjour les amis[a-T]je.veux[D.O.T.]be hello\nj'y vais xxx@yyy.net hello\nrehello flute.flute--[A-T]--je.vais{{D.o.t}}be salut";

// analyse de l'url
if ((isset($_POST['scan']) OR $_GET['url'] != '') and !$robot_possible) {

    $url = '';$url_post = '';
    // priorite au POST
    if (isset($_GET['url']) AND $_GET['url'] != '') {$url = $_GET['url'];}
    if (isset($_POST['url']) AND $_POST['url'] != '') {$url = $_POST['url'];$url_post = $_POST['url'];}
    unset($output);
    $output = array();

/******************************/
/* Moteur de décryptage start */
/******************************/
require ("config.inc.php");
// wget ?
if (file_exists($wget_path."/wget")) {
    exec($wget_path."/wget \"".$url."\" -O tmp 2>/dev/null", $output, $ret);
    if ($ret != 0) {die("<p><strong>".$wget_path."/wget a rencontr&eacute; un probl&egrave;me.</strong></p><p>Exit code: ".$ret.". Testez en console et consultez la documentation.<p><p>Voyez:</p><p> - <a href='http://www.gnu.org/software/wget/manual/html_node/Exit-Status.html'>wget exit codes</a>.</p><p> - <a href='http://www.tldp.org/LDP/abs/html/exitcodes.html'>Exit Codes With Special Meanings</a>.</p>");}
    }
else {
    die("<p><strong>Je ne trouve pas '".$wget_path."/wget'.</strong></p><p>La simulation de mailbot d&eacute;pend de wget. Si il est absent, vous devez l'installer pour que &ccedil;a fonctionne.<br />V&eacute;rifiez aussi le chemin de wget dans /mailbot/config.inc.php, variable '&#36;wget_path'.</p>");
    }

    $matches = array();

    $texte = file_get_contents('tmp');
    $texte_traite = html_entity_decode($texte);

    // parser les voyelles accentuées => A CORRIGER

    $a = "(a|A|&#65;|&#97;|á|&aacute;|&#225;|Á|&Aacute;|&#193;|â|&acirc;|&#226;|Â|&Acirc;|&#194;|à|&agrave;|&#224;|À|&Agrave;|&#192;|å|&aring;|&#229;|Å|&Aring;|&#197;|ã|&atilde;|&#227;|Ã|&Atilde;|&#195;|ä|&auml;|&#228;|Ä|&Auml;|&#196;|α|&alpha;|&#945;|Α|&Alpha;|&#913;)";
    $e = "(e|E|&#69;|&#101;|é|&eacute;|&#233;|É|&Eacute;|&#201;|ê|&ecirc;|&#234;|Ê|&Ecirc;|&#202;|è|&egrave;|&#232;|È&Egrave;|&#200;|ë|&euml;|&#235;|Ë|&Euml;|&#203;|ε|&epsilon;|&#949;|Ε|&Epsilon;|&#917;)";
    $i = "(i|I|&#73;|&#105;|í|&iacute;|&#237;|Í|&Iacute;|&#205;|î|&icirc;|&#238;|Î|&Icirc;|&#206;|ì|&igrave;|&#236;|Ì|&Igrave;|&#204;|ï|&iuml;|&#239;|Ï|&Iuml;|&#207;|ι|&iota;|&#953;|Ι|&Iota;|&#921;)";
    $o = "(o|O|&#79;|&#111;|ó|&oacute;|&#243;|Ó|&Oacute;|&#211;|ô|&ocirc;|&#244;|Ô|&Ocirc;|&#212;|ò|&ograve;|&#242;|Ò|&Ograve;|&#210;|ø|&oslash;|&#248;|Ø|&Oslash;|&#216;|õ|&otilde;|&#245;|Õ|&Otilde;|&#213;|ö|&ouml;|&#246;|Ö|&Ouml;|&#214;|ο|&omicron;|&#959;|Ο|&Omicron;|&#927;)";
    $u = "(u|U|&#85;|&#117;|ú|&uacute;|&#250;|Ú|&Uacute;|&#218;|û|&ucirc;|&#251;|Û|&Ucirc;|&#219;|ù|&ugrave;|&#249;|Ù|&Ugrave;|&#217;|ü|&uuml;|&#252;|Ü|&Uuml;|&#220;|υ|&upsilon;|&#965;|Υ|&Upsilon;|&#933;)";

/*
    $a = "[@aàâäæÂÄåÁÅ]";
    $e = "[eéèêëÊËÒ]";
    $i = "[iîï¡ÌÏÍÎ]";
    $o = "[oôöºÒøØÔÓΩÖ]";
    $u = "[uùûüÛÜÚÙ]";
*/
    // match at, chez, arobaz, arobase découpé entre signes non alphanum

    $at = $a."+[\W_]*t+";
    $chez = "c+[\W_]*h+[\W_]*".$e."+[\W_]*z+";
    $arobase = $a."+[\W_]*r+[\W_]*".$o."+[\W_]*b+[\W_]*".$a."+[\W_]*(s|z)+[\W_]*(".$e."|)+";
    $motif = "/( ?[\W_]+(".$at."|".$chez."|".$arobase.")[\W_]+ ?|@)/i";
    $at_motif = $motif;

    $remplace = "@";
    $texte_traite = preg_replace($motif, $remplace, $texte_traite);

    // match dot, point, punt découpé entre signes non alphanum
    $dot = "d+[\W_]*".$o."+[\W_]*t+";
    $point = "p+[\W_]*".$o."+[\W_]*".$i."+[\W_]*n+[\W_]*t+";
    $punt = "p+[\W_]*".$u."+[\W_]*n+[\W_]*t+";
    $motif = "/( ?[\W_]+(".$dot."|".$point."|".$punt.")[\W_]+ ?|\.)/i";
    $dot_motif = $motif;

//$motif = "/( ?[\W_]+(\.)[\W_]+ ?|\.)/i";
    $remplace = ".";
    $texte_traite = preg_replace($motif, $remplace, $texte_traite);

//$texte_traite = preg_replace($motif, $remplace, $texte_traite, -1,$compte);
//echo $compte." + ".$motif." - ".$at." - ".$chez." -".$arobase."<hr>";
//echo "<h1>texte traite</h1>".$texte_traite."<hr>";


//echo "<h1>texte</h1>".$texte."<h1>texte traite</h1>".$texte_traite."<hr>";

    //match mails dans un texte:
    $motif = "/[\w.-]+@[\w.-]{2,}\.[a-zA-Z]{2,}/";
    preg_match_all($motif, $texte_traite, $matches, PREG_PATTERN_ORDER);
}

//print_r ($matches);
//echo "<hr>";

/****************************/
/* Moteur de décryptage end */
/****************************/


?>
<html>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="fr">
<head>
<?php include ("../commun/inc-meta.php"); ?>
<style>
table, th, td {border: 1px dotted;border-collapse:collapse;padding 5px;}
td {padding: 5px;}
</style>
</head>
<body>
<?php include ("../commun/inc-entete.php"); ?>
<div class="encart"><h2><a href='../accueil' title='Accueil Aie confiance!'>Aie confiance !</a></h2><p><img src="../commun/big-brother-reduit.jpg"></p></div>

<?php
if (!$robot_possible) { // Si cookie EtreHumain ..
?>
<div><a href="index.php?show_source=true">Code source du formulaire</a></div>
<?php } ?>

<h1>Collecteur d'adresses email</h1>

<div>Croire que les spambots ne rep&egrave;rent pas facilement des emails encod&eacute;s sous le type user{{a+T])domain(p-oi_nt]]tld ou encore sous le type u&amp;#115;er aT do&amp;#109;a&amp;#105;n DoT tld ?</div>

<?php
// Si pas de cookie EtreHumain ..
if ($robot_possible) {
?>

<h2>Mais tout d'abord : Etes-vous bien un être humain ?</h2>
<div>Oui ? Alors prouvez le en répondant à la question ci-dessous :</div>
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
    <?php echo " ".$un." + ".$deux." = "; ?><input name="captcha" type="text" size="30" value=""><br>
    <div class="note">Il s'agit ici de ne pas permettre à des robots d'utiliser ce script pour collecter des adresses email. Répondre à la question posée placera un cookie sur votre ordinateur, cookie qui vous évitera de devoir répondre à nouveau à cette question, tant que le cookie sera présent (On suppose donc que votre navigateur accepte les cookies!). A partir de ce moment, vous pouvez aussi passer la page à scanner via l'url de cette manière: <?php echo "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME']; ?>?url=http://domain.tld/page_a_scanner</div>
    <input type="submit" name="verif" value=" Je ne suis pas un robot "><br><br>
    <input type="hidden" name="un" value="<?php echo $un; ?>">
    <input type="hidden" name="deux" value="<?php echo $deux; ?>">
</form>

<?php
} else { // sinon, si cookie EtreHumain
?>

<div>Essayez en entrant une url ci dessous et en scannant la page:</div>

<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
    <input name="url" type="text" size="30" value="<?php echo $url_post; ?>"><br><br>
    <!-- <textarea name="texte" cols="50" rows="10"><?php //echo $texte; ?></textarea><br><br> -->
    <input type="submit" name="scan" value=" Scanner "><br><br>
</form>

<h2>Vous êtes apparemment bien un être humain.</h2>
<div class="note">Du moins si j'en crois le cookie présent sur votre ordinateur (<?php echo "Nom: ".$nom_cookie_robot." Valeur: ".$_COOKIE[$nom_cookie_robot]; ?>) ;). Notez : Vous pouvez maintenant aussi passer la page à scanner via l'url de cette manière: <?php echo "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME']; ?>?url=http://domain.tld/page_a_scanner</div>

<!-- fonctions page de test test.php -->
<div><a href="test.php">Page de test</a> - <a href="index.php?url=<?php echo "http://".$_SERVER['HTTP_HOST'].substr($_SERVER['SCRIPT_NAME'], 0, strrpos($_SERVER['SCRIPT_NAME'], "/")); ?>/test.php">Tester la page de test</a> - <a href="test.php?show_source=true">Code source de la page de test</a></div>

<?php
}
?>

<div>
<?php
// afficher les résultats si il y en a (et il n'y en a que si le cookie EtreHumain est en place
if ((isset($_POST['scan']) OR $_GET['url'] != '') and count($matches) > 0) {
    echo "<h2>Scan de <a href='".$url."'>".$url."</a></h2>";
    echo "<div style='font-style:italic;font-size:90%;'>Note importante: Le script est limit&eacute; au scan d'une seule page, n'enregistre rien, fait juste un affichage de ses d&eacute;couvertes. Mais il ne faudrait pas coder beaucoup pour l'envoyer se balader sur le net et en faire un m&eacute;chant petit spambot ;(</div>\n";
    echo "<ul>\n";
    $i = 0;
    foreach ($matches as $val) {
        foreach ($val as $val2) {
            echo "<li>".$val2."</li>\n";
            $i++;
        }
    }
    echo "</ul>\n";
    echo $i." adresse(s) email trouv&eacute;e(s)";

?>

<hr>
<h2>Infos complémentaires:</h2>
<h3>Fonctionnement</h3>
<ul>
<li>Le robot cherche les "at, chez, arobaz ou arobase" et les "dot, point ou punt", découpés ou non, entre signes non alphanumériques</li>
<li>Il analyse les items trouvés et les retransforme en adresses email.</li>
<li>Le robot est limité à une page.</li>
<li><b>Il n'enregistre rien;</b> il se contente d'afficher ses résultats.</li>
</ul>

<h3>Améliorations possibles (et vivement déconseillées)</h3>
<ul>
    <li>Etablir une dictionnaire pour augmenter le nombre de motifs de recherche (les "at, chez, arobaz ou arobase" et les "dot, point ou punt")</li>
    <li>Débrider le robot pour qu'il commence à parcourir le net et lui adjoindre une bse de données; mais là, on a une vraie méchanceté.</li>
</ul>

<h3>Motifs.</h3>
<h4>at, chez, arobaz ou arobase</h4>
<table>
<tr style="vertical-align:top;border: 1px dotted;"><td>at&nbsp;=&nbsp;</td><td>(a|A|A|a|á|á|á|Á|Á|Á|â|â|â|Â|Â|Â|à|à|à|À|À|À|å|å|å|Å|Å|Å|ã|ã|ã|Ã|Ã|Ã|ä|ä|ä|Ä|Ä|Ä|α|α|α|Α|Α|Α)+[\W_]*t+</td></tr>
<tr style="vertical-align:top;border: 1px dotted;"><td>chez&nbsp;=&nbsp;</td><td>c+[\W_]*h+[\W_]*(e|E|E|e|é|é|é|É|É|É|ê|ê|ê|Ê|Ê|Ê|è|è|è|ÈÈ|È|ë|ë|ë|Ë|Ë|Ë|ε|ε|ε|Ε|Ε|Ε)+[\W_]*z+</td></tr>
<tr style="vertical-align:top;border: 1px dotted;"><td>arobaz ou arobase&nbsp;=&nbsp;</td><td>(a|A|A|a|á|á|á|Á|Á|Á|â|â|â|Â|Â|Â|à|à|à|À|À|À|å|å|å|Å|Å|Å|ã|ã|ã|Ã|Ã|Ã|ä|ä|ä|Ä|Ä|Ä|α|α|α|Α|Α|Α)+[\W_]*r+[\W_]*(o|O|O|o|ó|ó|ó|Ó|Ó|Ó|ô|ô|ô|Ô|Ô|Ô|ò|ò|ò|Ò|Ò|Ò|ø|ø|ø|Ø|Ø|Ø|õ|õ|õ|Õ|Õ|Õ|ö|ö|ö|Ö|Ö|Ö|ο|ο|ο|Ο|Ο|Ο)+[\W_]*b+[\W_]*(a|A|A|a|á|á|á|Á|Á|Á|â|â|â|Â|Â|Â|à|à|à|À|À|À|å|å|å|Å|Å|Å|ã|ã|ã|Ã|Ã|Ã|ä|ä|ä|Ä|Ä|Ä|α|α|α|Α|Α|Α)+[\W_]*(s|z)+[\W_]*((e|E|E|e|é|é|é|É|É|É|ê|ê|ê|Ê|Ê|Ê|è|è|è|ÈÈ|È|ë|ë|ë|Ë|Ë|Ë|ε|ε|ε|Ε|Ε|Ε)|)+</td></tr>
</table>
<div style="font-style:italic;">où "(a|A|A|a|á|á|á|Á|Á|Á .. etc." = "(a|A|&amp;#65;|&amp;#97;|á|&amp;aacute;|&amp;#225;|Á|&amp;Aacute;|&amp;#193; .. etc."</div>

<h4>dot, point ou punt</h4>
<table>
<tr style="vertical-align:top;border: 1px dotted;"><td>dot&nbsp;=&nbsp;</td><td>d+[\W_]*(o|O|O|o|ó|ó|ó|Ó|Ó|Ó|ô|ô|ô|Ô|Ô|Ô|ò|ò|ò|Ò|Ò|Ò|ø|ø|ø|Ø|Ø|Ø|õ|õ|õ|Õ|Õ|Õ|ö|ö|ö|Ö|Ö|Ö|ο|ο|ο|Ο|Ο|Ο)+[\W_]*t+</td></tr>
<tr style="vertical-align:top;border: 1px dotted;"><td>point&nbsp;=&nbsp;</td><td>p+[\W_]*(o|O|O|o|ó|ó|ó|Ó|Ó|Ó|ô|ô|ô|Ô|Ô|Ô|ò|ò|ò|Ò|Ò|Ò|ø|ø|ø|Ø|Ø|Ø|õ|õ|õ|Õ|Õ|Õ|ö|ö|ö|Ö|Ö|Ö|ο|ο|ο|Ο|Ο|Ο)+[\W_]*(i|I|I|i|í|í|í|Í|Í|Í|î|î|î|Î|Î|Î|ì|ì|ì|Ì|Ì|Ì|ï|ï|ï|Ï|Ï|Ï|ι|ι|ι|Ι|Ι|Ι)+[\W_]*n+[\W_]*t+</td></tr>
<tr style="vertical-align:top;border: 1px dotted;"><td>punt&nbsp;=&nbsp;</td><td>p+[\W_]*(u|U|U|u|ú|ú|ú|Ú|Ú|Ú|û|û|û|Û|Û|Û|ù|ù|ù|Ù|Ù|Ù|ü|ü|ü|Ü|Ü|Ü|υ|υ|υ|Υ|Υ|Υ)+[\W_]*n+[\W_]*t+</td></tr>
</table>
<div style="font-style:italic;">et où "(o|O|O|o|ó|ó|ó|Ó|Ó|Ó .. etc." = "(o|O|&amp;#79;|&amp;#111;|ó|&amp;oacute;|&amp;#243;|Ó|&amp;Oacute;|&amp;#211; .. etc."</div>

<?php } // if ((isset($_POST['scan']) OR $_GET['url'] != '') and count($matches) > 0) {?>


<?php include ("../commun/inc-pied.php"); ?>

</div>

</body>
</html>
