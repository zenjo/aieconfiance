# Structure du site

## Résumé
- accueil
- commun
    - js
- mailbot
- tracking
    - api
    - images
    - include
---
## Description
- *index.php* renvoie vers acceuil

### accueil
- *index.php*
	- brève présentation
	- liens vers les applications tracking, mailbot, ...
	- liens vers des documentations externes

### commun
Images et scripts qui se retrouvent dans toutes les applications. Les noms de fichiers parlent d'eux-mêmes.

- *inc-meta.php*
- *inc-entete.php*
- *inc-pied.php*
- diverses images

#### js 
répertoire contenant le javascript

- librairie jquery
- *jquery-fonctions-communes.js* fonctions propres au site

### mailbot

- *index.php* page principale (traqueur) du mailbot
- *config.inc.php* chemin de l'utilitaire wget (différent sous mac et linux)
- *test.php* la page de test
- *tmp* le fichier temporaire d'enregistrement du source des pages analysées (le fichier et son répertoire parent necessite un accès en écriture pour le serveur web) 

### tracking
- *tracking.php* page principale, résumé des traces
- *page.php* les pages blue, red, green
- *reinit.php* réinitialisation des données à 0 (effacement des cookies)
- *deconnexion.php* effacement des cookies nom, prenom, email, mais pas du reste (et surtout pas de id!)
- *remote-tracking-simulation.php* fichier de script de la webbeacon, qui envoie ses données dans un fichier texte donnees_collectees.txt situé en interne. Dans la présente: sebille.name -> sebille.name
- *remote-tracking-simulation-externe.php* fichier de script de la webbeacon, situé sur un serveur distant, qui envoie ses données dans un fichier texte donnees_collectees_externe.txt situé également sur le même serveur distant. Dans la présente: sebille.name -> cassiopea.org
-  les fichiers *donnees_collectees.txt* et *donnees_collectees_externe.txt* et leurs répertoires parents necessitent un accès en écriture pour le serveur web.

#### api
*k*, clé pour la géolocalisation google. (vide sur git)
#### images
images pour l'outil tracking
#### include
- *inc-coords.php* (*page.php, tracking.php*) : récupère les cookies de nom, prenom, email si ils existent

- *inc-page_tracking.php* (*page.php*) : si on est inscrit, gère le cookies des préférences par page bleue, rouge ou verte. Fournit $nbvisitepage_pref = le nombre de visite d'une page

- *inc-navigation.php* (*page.php, tracking.php*) : fournit la navigation générale dans le site

- *inc-tracking.php* (*tracking.php*) : gère et fournit le temps, le changement éventuel de nom, le nombre de visites et les place dans les cookies

- *inc-preferences.php* (*page.php, tracking.php*) : fournit et gère l'affichage des préférences générales de navigation entre les pages bleue, rouge ou verte

